package com.geekhub.web.search;

import com.geekhub.repository.dao.impl.mappers.PictureListRowMapper;
import com.geekhub.repository.entity.Picture;
import com.geekhub.repository.entity.SearchForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class SearchEngine {

    private final JdbcTemplate jdbcTemplate;
    private final PictureListRowMapper pictureListRowMapper;

    @Autowired
    public SearchEngine(JdbcTemplate jdbcTemplate, PictureListRowMapper pictureListRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.pictureListRowMapper = pictureListRowMapper;
    }

    public List<Picture> getAndSearch(SearchForm searchForm) {
        List<Map<String, Object>> rows;
        String sql = "SELECT * FROM PUBLIC.PICTURE WHERE ";

        String name = searchForm.getName();
        String style = searchForm.getStyle();
        Double price = searchForm.getPrice();
        
        if (name == null && style == null && price == null) {
            throw new NullPointerException("Search form is hollow");
        }

        if (name != null) {
            sql = sql.concat("NAME='" + name + "' AND ");
        }
        if (style != null) {
            sql = sql.concat("STYLE='" + style + "' AND ");
        }
        if (price != null) {
            sql = sql.concat("PRICE=" + price + " AND ");
        }

        sql = sql.substring(0, sql.length() - 5);
        rows = jdbcTemplate.queryForList(sql);

        return pictureListRowMapper.getPictureList(rows);
    }

    public List<Picture> getOrSearch(SearchForm searchForm) {
        List<Map<String, Object>> rows;
        List<Picture> orPictures = new ArrayList<>();
        String sql;

        String name = searchForm.getName();
        String style = searchForm.getStyle();
        Double price = searchForm.getPrice();

        if (name != null) {
            sql = "SELECT * FROM PUBLIC.PICTURE WHERE NAME='" + name + "'";
            rows = jdbcTemplate.queryForList(sql);
            List<Picture> nameList = pictureListRowMapper.getPictureList(rows);
            orPictures.addAll(nameList);
        }
        if (style != null) {
            sql = "SELECT * FROM PUBLIC.PICTURE WHERE STYLE='" + style + "'";
            rows = jdbcTemplate.queryForList(sql);
            List<Picture> styleList = pictureListRowMapper.getPictureList(rows);
            orPictures.addAll(styleList);
        }
        if (price != null) {
            sql = "SELECT * FROM PUBLIC.PICTURE WHERE PRICE=" + price;
            rows = jdbcTemplate.queryForList(sql);
            List<Picture> priceList = pictureListRowMapper.getPictureList(rows);
            orPictures.addAll(priceList);
        }
        
        return orPictures;
    }
}
