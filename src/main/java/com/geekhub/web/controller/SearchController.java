package com.geekhub.web.controller;

import com.geekhub.repository.entity.Picture;
import com.geekhub.repository.entity.SearchForm;
import com.geekhub.web.search.SearchEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class SearchController {

    private final SearchEngine searchEngine;

    @Autowired
    public SearchController(SearchEngine searchEngine) {
        this.searchEngine = searchEngine;
    }

    @GetMapping("/search")
    public String search(Model model) {
        model.addAttribute("searchForm", new SearchForm());
        return "advanced_search";
    }

    @PostMapping("/search")
    public String searchPost(@ModelAttribute("searchForm") SearchForm searchForm,
                             BindingResult result,
                             Model model) {
        if (result.hasErrors()) {
            return "redirect:/error";
        }

        List<Picture> searchedPictures;

        if (searchForm.getPriority().equals("AND")) {
            searchedPictures = searchEngine.getAndSearch(searchForm);
        } else {
            searchedPictures = searchEngine.getOrSearch(searchForm);
        }

        model.addAttribute("searchResult", searchedPictures);
        return "search_result";
    }
}
