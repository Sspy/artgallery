package com.geekhub.web.controller;

import com.geekhub.repository.dao.impl.PainterJdbcDao;
import com.geekhub.repository.entity.Painter;
import com.geekhub.web.tool.Page;
import com.geekhub.web.tool.PageRequest;
import com.geekhub.web.tool.PaginationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class PaintersController {

    private final PainterJdbcDao painterJdbcDao;

    private static final int PAINTERS_PER_PAGE = 6;

    @Autowired
    public PaintersController(PainterJdbcDao painterJdbcDao) {
        this.painterJdbcDao = painterJdbcDao;
    }

    @GetMapping("/painters")
    public String painters(@RequestParam("page") int page, Model model) {
        PageRequest request = new PageRequest(page, PAINTERS_PER_PAGE);
        List<Painter> all = painterJdbcDao.findAll();
        Page<Painter> painterPage = PaginationUtils.getPage(all, request);

        model.addAttribute("pageNum", painterPage.getPage());
        model.addAttribute("painterList", painterPage.getItems());
        return "painters";
    }

    @GetMapping("/painters/{id}")
    public String painter(@PathVariable("id") int id, Model model) {
        Painter painter = painterJdbcDao.findById(id);

        model.addAttribute("painter", painter);
        return "painter_info";
    }
}
