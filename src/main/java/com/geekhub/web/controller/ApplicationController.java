package com.geekhub.web.controller;

import com.geekhub.repository.dao.impl.NewsJdbcDao;
import com.geekhub.repository.entity.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.util.List;

@Controller
public class ApplicationController {

    @Value("${param.error}") String error;

    private final NewsJdbcDao newsJdbcDao;

    @Autowired
    public ApplicationController(NewsJdbcDao newsJdbcDao) {
        this.newsJdbcDao = newsJdbcDao;
    }

    @GetMapping("/")
    public String home(Model model) {
        List<News> homePageNewsList = newsJdbcDao.getHotNews();
        model.addAttribute("newsList", homePageNewsList);
        return "index";
    }

    @GetMapping("/error")
    public String error(Model model) {
        model.addAttribute("error", error);
        return "error";
    }
}
