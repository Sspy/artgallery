package com.geekhub.web.controller;

import com.geekhub.repository.entity.UserForm;
import com.geekhub.repository.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class AuthController {

    private final UserService userService;

    @Autowired
    public AuthController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/login")
    public String login() {
        return "sign-in";
    }

    @GetMapping("/registration")
    public String registration(Model model) {
        model.addAttribute("userForm", new UserForm());

        return "registration_page";
    }

    @PostMapping("/registration")
    public String registration(@ModelAttribute("userForm") UserForm userForm,
                               BindingResult result) {
        if (result.hasErrors()) {
            return "redirect:/error";
        }

        userService.save(userForm);

        return "redirect:/";
    }
}
