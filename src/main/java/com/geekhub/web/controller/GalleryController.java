package com.geekhub.web.controller;

import com.geekhub.repository.dao.impl.PictureJdbcDao;
import com.geekhub.repository.entity.Picture;
import com.geekhub.web.tool.Page;
import com.geekhub.web.tool.PageRequest;
import com.geekhub.web.tool.PaginationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class GalleryController {

    private final PictureJdbcDao pictureJdbcDao;

    private static final int PICTURE_PER_PAGE = 6;

    @Autowired
    public GalleryController(PictureJdbcDao pictureJdbcDao) {
        this.pictureJdbcDao = pictureJdbcDao;
    }

    @GetMapping("/gallery")
    public String gallery() {
        return "gallery_styles";
    }

    @GetMapping("/gallery/{style}")
    public String style(@PathVariable("style") String style,
                        @RequestParam("page") int page,
                        Model model) {
        List<Picture> picturesList = pictureJdbcDao.findByStyle(style.toLowerCase());
        PageRequest pageRequest = new PageRequest(page, PICTURE_PER_PAGE);
        Page<Picture> picturePage = PaginationUtils.getPage(picturesList, pageRequest);

        model.addAttribute("pageNum", picturePage.getPage());
        model.addAttribute("pictureList", picturePage.getItems());
        return "gallery_pictures";
    }

    @GetMapping("/gallery/pic{id}")
    public String singlePicture(@PathVariable("id") int id, Model model) {
        Picture picture = pictureJdbcDao.findById(id);

        model.addAttribute("picture", picture);
        return "pictures-single";
    }
}