package com.geekhub.web.controller;

import com.geekhub.repository.dao.impl.NewsJdbcDao;
import com.geekhub.repository.entity.News;
import com.geekhub.web.tool.Page;
import com.geekhub.web.tool.PageRequest;
import com.geekhub.web.tool.PaginationUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Controller
public class NewsController {

    private static final int NEWS_PER_PAGE = 10;

    private final NewsJdbcDao newsJdbcDao;

    @Autowired
    public NewsController(NewsJdbcDao newsJdbcDao) {
        this.newsJdbcDao = newsJdbcDao;
    }

    @GetMapping("/news")
    public String news(@RequestParam("page") int pageNum, Model model) {
        PageRequest pageRequest = new PageRequest(pageNum, NEWS_PER_PAGE);
        List<News> allNews = newsJdbcDao.findAll();
        Page<News> page = PaginationUtils.getPage(allNews, pageRequest);
        List<News> newsList = new ArrayList<>(page.getItems());

        model.addAttribute("pageNum", page.getPage());
        model.addAttribute("newsList", newsList);
        return "news";
    }

    @GetMapping("/news/{id}")
    public String singleNews(@PathVariable("id") int id, Model model) {
        News news = newsJdbcDao.findById(id);

        model.addAttribute("news", news);
        return "news-single";
    }
}
