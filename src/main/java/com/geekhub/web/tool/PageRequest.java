package com.geekhub.web.tool;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Setter
@Getter
@AllArgsConstructor
public class PageRequest {

    private final int page;
    private final int perPage;
}
