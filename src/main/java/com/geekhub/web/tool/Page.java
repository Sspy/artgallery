package com.geekhub.web.tool;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Setter
@Getter
@AllArgsConstructor
public class Page<E> {

    private final int page;
    private final int perPage;
    private final List<E> items;
}