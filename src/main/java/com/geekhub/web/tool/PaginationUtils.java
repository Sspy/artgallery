package com.geekhub.web.tool;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.List;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class PaginationUtils {

    public static <E> Page<E> getPage(List<E> items, PageRequest pageRequest) {
        int size = items.size();
        int page = pageRequest.getPage();
        int limiter = pageRequest.getPerPage();
        int pageCounter;
        List<E> currentItems;

        if (size % limiter == 0) {
            pageCounter = size / limiter;
        } else {
            pageCounter = size / limiter + 1;
        }

        if (size < limiter) {
            currentItems = items;
        } else {
            currentItems = items.subList((page - 1) * limiter, page * limiter);
        }

        return new Page(page, pageCounter, currentItems);
    }
}

