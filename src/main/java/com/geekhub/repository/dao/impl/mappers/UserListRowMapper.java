package com.geekhub.repository.dao.impl.mappers;

import com.geekhub.repository.entity.User;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class UserListRowMapper {

    public List<User> getUserList(List<Map<String, Object>> rows) {
        List<User> userList = new ArrayList<>();
        for (Map row : rows) {
            User user = new User();

            user.setId((Integer) row.get("ID"));
            user.setUsername((String) row.get("USERNAME"));
            user.setPassword((String) row.get("PASSWORD"));
            user.setEmail((String) row.get("EMAIL"));
            user.setRole((String) row.get("ROLE"));

            userList.add(user);
        }
        return userList;
    }
}
