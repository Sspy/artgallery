package com.geekhub.repository.dao.impl;

import com.geekhub.repository.dao.PainterDao;
import com.geekhub.repository.dao.impl.mappers.PainterListRowMapper;
import com.geekhub.repository.dao.impl.mappers.PainterRowMapper;
import com.geekhub.repository.entity.Painter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PainterJdbcDao implements PainterDao {

    private final JdbcTemplate jdbcTemplate;
    private final PainterListRowMapper painterListRowMapper;

    @Autowired
    public PainterJdbcDao(JdbcTemplate jdbcTemplate, PainterListRowMapper painterListRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.painterListRowMapper = painterListRowMapper;
    }

    @Override
    public void insert(Painter painter) {
        jdbcTemplate.update(
                "INSERT INTO PUBLIC.PAINTER (ID, NAME, STYLE, IMAGE, FULL_IMAGE, BIRTH, DEAD) VALUES ("
                        + painter.getId() + ", '"
                        + painter.getName() + "', '"
                        + painter.getStyle() + "', '"
                        + painter.getImage() + "', '"
                        + painter.getFullImage() + "', "
                        + painter.getBirth() + ", "
                        + painter.getDead() + ")"
        );
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM PUBLIC.PAINTER WHERE ID=" + id);
    }

    @Override
    public List<Painter> findAll() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.PAINTER");
        return painterListRowMapper.getPainterList(rows);
    }

    @Override
    public Painter findById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM PUBLIC.PAINTER WHERE ID=" + id,
                new Object[]{id},
                new PainterRowMapper()
        );
    }
}
