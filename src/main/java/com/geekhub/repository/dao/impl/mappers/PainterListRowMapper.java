package com.geekhub.repository.dao.impl.mappers;

import com.geekhub.repository.entity.Painter;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class PainterListRowMapper {

    public List<Painter> getPainterList(List<Map<String, Object>> rows) {
        ArrayList<Painter> painters = new ArrayList<>();
        for (Map row : rows) {
            Painter painter = new Painter();

            painter.setId((Integer) row.get("ID"));
            painter.setName((String) row.get("NAME"));
            painter.setStyle((String) row.get("STYLE"));
            painter.setImage((String) row.get("IMAGE"));
            painter.setFullImage((String) row.get("FULL_IMAGE"));
            painter.setBirth(((Date) row.get("BIRTH")).toLocalDate());
            painter.setDead(((Date) row.get("DEAD")).toLocalDate());

            painters.add(painter);
        }
        return  painters;
    }
}
