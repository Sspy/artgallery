package com.geekhub.repository.dao.impl.mappers;

import com.geekhub.repository.entity.News;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class NewsRowMapper implements RowMapper<News> {

    @Override
    public News mapRow(ResultSet rs, int rowNum) throws SQLException {
        News news = new News();

        news.setId(rs.getInt("ID"));
        news.setName(rs.getString("NAME"));
        news.setReview(rs.getString("REVIEW"));
        news.setText(rs.getString("TEXT"));
        news.setImage(rs.getString("IMAGE"));
        news.setFullImage(rs.getString("FULL_IMAGE"));
        news.setDate(rs.getDate("DATE").toLocalDate());

        return news;
    }
}

