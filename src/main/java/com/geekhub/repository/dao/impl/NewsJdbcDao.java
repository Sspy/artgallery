package com.geekhub.repository.dao.impl;

import com.geekhub.repository.dao.NewsDao;
import com.geekhub.repository.dao.impl.mappers.NewsListRowMapper;
import com.geekhub.repository.dao.impl.mappers.NewsRowMapper;
import com.geekhub.repository.entity.News;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class NewsJdbcDao implements NewsDao {

    private final JdbcTemplate jdbcTemplate;
    private final NewsListRowMapper newsListRowMapper;

    @Autowired
    public NewsJdbcDao(JdbcTemplate jdbcTemplate, NewsListRowMapper newsListRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.newsListRowMapper = newsListRowMapper;
    }

    @Override
    public void insert(News news) {
        this.jdbcTemplate.update(
                "INSERT INTO PUBLIC.NEWS (ID, NAME, REVIEW, TEXT, IMAGE, DATE) VALUES ("
                        + news.getId() + ", '"
                        + news.getName() + "', '"
                        + news.getReview() + "', '"
                        + news.getText() + "', '"
                        + news.getImage() + "', "
                        + news.getDate() + ")"
        );
    }

    @Override
    public void delete(int id) {
        this.jdbcTemplate.update("DELETE FROM PUBLIC.NEWS WHERE ID=" + id);
    }

    @Override
    public List<News> findAll() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.NEWS");
        return this.newsListRowMapper.getNewsList(rows);
    }

    @Override
    public News findById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM PUBLIC.NEWS WHERE ID=" + id,
                new Object[]{id},
                new NewsRowMapper()
        );
    }

    public List<News> getHotNews() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.NEWS LIMIT 4");
        return this.newsListRowMapper.getNewsList(rows);
    }
}
