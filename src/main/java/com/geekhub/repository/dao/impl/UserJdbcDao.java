package com.geekhub.repository.dao.impl;

import com.geekhub.repository.dao.UserDao;
import com.geekhub.repository.dao.impl.mappers.UserListRowMapper;
import com.geekhub.repository.dao.impl.mappers.UserRowMapper;
import com.geekhub.repository.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class UserJdbcDao implements UserDao {

    private final JdbcTemplate jdbcTemplate;
    private final UserListRowMapper userListRowMapper;

    @Autowired
    public UserJdbcDao(JdbcTemplate jdbcTemplate, UserListRowMapper userListRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.userListRowMapper = userListRowMapper;
    }

    @Override
    public void save(User user) {
        jdbcTemplate.update("INSERT INTO PUBLIC.USER (ID, USERNAME, PASSWORD, EMAIL, ROLE) VALUES ("
                + user.getId() + ", '"
                + user.getUsername() + "', '"
                + user.getPassword() + "', '"
                + user.getEmail() + "', '"
                + user.getRole() + "')"
        );
    }

    @Override
    public void delete(int id) {
        jdbcTemplate.update("DELETE FROM PUBLIC.USER WHERE ID=" + id);
    }

    @Override
    public List<User> findAll() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.USER");
        return userListRowMapper.getUserList(rows);
    }

    @Override
    public User findByUsername(String username) {
        return jdbcTemplate.queryForObject("SELECT * FROM PUBLIC.USER WHERE USERNAME='" + username + "'",
                new Object[]{username},
                new UserRowMapper()
        );
    }
}
