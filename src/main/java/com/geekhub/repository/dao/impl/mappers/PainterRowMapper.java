package com.geekhub.repository.dao.impl.mappers;

import com.geekhub.repository.entity.Painter;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PainterRowMapper implements RowMapper<Painter> {

    @Override
    public Painter mapRow(ResultSet rs, int rowNum) throws SQLException {
        Painter painter = new Painter();

        painter.setId(rs.getInt("ID"));
        painter.setName(rs.getString("NAME"));
        painter.setStyle(rs.getString("STYLE"));
        painter.setImage(rs.getString("IMAGE"));
        painter.setFullImage(rs.getString("FULL_IMAGE"));
        painter.setBirth(rs.getDate("BIRTH").toLocalDate());
        painter.setDead(rs.getDate("DEAD").toLocalDate());

        return painter;
    }
}
