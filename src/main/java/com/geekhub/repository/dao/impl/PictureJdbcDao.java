package com.geekhub.repository.dao.impl;

import com.geekhub.repository.dao.PictureDao;
import com.geekhub.repository.dao.impl.mappers.PictureListRowMapper;
import com.geekhub.repository.dao.impl.mappers.PictureRowMapper;
import com.geekhub.repository.entity.Picture;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

@Repository
public class PictureJdbcDao implements PictureDao {

    private final JdbcTemplate jdbcTemplate;
    private final PictureListRowMapper pictureListRowMapper;

    @Autowired
    public PictureJdbcDao(JdbcTemplate jdbcTemplate, PictureListRowMapper pictureListRowMapper) {
        this.jdbcTemplate = jdbcTemplate;
        this.pictureListRowMapper = pictureListRowMapper;
    }

    @Override
    public void insert(Picture picture) {
        this.jdbcTemplate.update(
                "INSERT INTO PUBLIC.PICTURE (ID, NAME, IMAGE, FULL_IMAGE, PAINTER, STYLE, PRICE) VALUES ("
                        + picture.getId() + ", '"
                        + picture.getName() + "', '"
                        + picture.getImage() + "', '"
                        + picture.getFullImage() + "', '"
                        + picture.getPainter() + "', '"
                        + picture.getStyle() + "', "
                        + picture.getPrice() + ")"
        );
    }

    @Override
    public void delete(int id) {
        this.jdbcTemplate.update("DELETE FROM PUBLIC.PICTURE WHERE ID=" + id);
    }

    @Override
    public List<Picture> findAll() {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.PICTURE");
        return pictureListRowMapper.getPictureList(rows);
    }

    @Override
    public Picture findById(int id) {
        return jdbcTemplate.queryForObject(
                "SELECT * FROM PUBLIC.PICTURE WHERE ID=" + id,
                new Object[]{id},
                new PictureRowMapper()
        );
    }

    @Override
    public List<Picture> findByStyle(String style) {
        List<Map<String, Object>> rows = jdbcTemplate.queryForList("SELECT * FROM PUBLIC.PICTURE WHERE STYLE='" + style + "'");
        return pictureListRowMapper.getPictureList(rows);
    }
}
