package com.geekhub.repository.dao.impl.mappers;


import com.geekhub.repository.entity.Picture;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class PictureListRowMapper {

    public List<Picture> getPictureList(List<Map<String, Object>> rows) {
        List<Picture> pictureList = new ArrayList<>();
        for (Map row : rows) {
            Picture picture = new Picture();

            picture.setId((int) row.get("ID"));
            picture.setName((String) row.get("NAME"));
            picture.setImage((String) row.get("IMAGE"));
            picture.setFullImage((String) row.get("FULL_IMAGE"));
            picture.setPainter((String) row.get("PAINTER"));
            picture.setStyle((String) row.get("STYLE"));
            picture.setPrice((double) row.get("PRICE"));

            pictureList.add(picture);
        }
        return pictureList;
    }
}
