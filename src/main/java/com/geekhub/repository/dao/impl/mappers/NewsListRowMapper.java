package com.geekhub.repository.dao.impl.mappers;

import com.geekhub.repository.entity.News;
import org.springframework.stereotype.Component;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Component
public class NewsListRowMapper {

    public List<News> getNewsList(List<Map<String, Object>> rows) {
        List<News> newsList = new ArrayList<>();
        for (Map row : rows) {
            News news = new News();

            news.setId((int) row.get("ID"));
            news.setName((String) row.get("NAME"));
            news.setReview((String) row.get("REVIEW"));
            news.setText((String) row.get("TEXT"));
            news.setImage((String) row.get("IMAGE"));
            news.setFullImage((String) row.get("FULL_IMAGE"));
            news.setDate(((Date) row.get("DATE")).toLocalDate());

            newsList.add(news);
        }
        return newsList;
    }
}
