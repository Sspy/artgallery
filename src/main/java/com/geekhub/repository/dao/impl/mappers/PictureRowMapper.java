package com.geekhub.repository.dao.impl.mappers;


import com.geekhub.repository.entity.Picture;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class PictureRowMapper implements RowMapper<Picture> {
    
    @Override
    public Picture mapRow(ResultSet rs, int rowNum) throws SQLException {
        Picture picture = new Picture();

        picture.setId(rs.getInt("ID"));
        picture.setName(rs.getString("NAME"));
        picture.setImage(rs.getString("IMAGE"));
        picture.setFullImage(rs.getString("FULL_IMAGE"));
        picture.setPainter(rs.getString("PAINTER"));
        picture.setStyle(rs.getString("STYLE"));
        picture.setPrice(rs.getDouble("PRICE"));
        
        return picture;
    }
}
