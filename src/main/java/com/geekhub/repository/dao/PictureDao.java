package com.geekhub.repository.dao;

import com.geekhub.repository.entity.Picture;

import java.util.List;

public interface PictureDao {

    void insert(Picture picture);

    void delete(int id);

    List<Picture> findAll();

    Picture findById(int id);

    List<Picture> findByStyle(String style);
}
