package com.geekhub.repository.dao;


import com.geekhub.repository.entity.Painter;

import java.util.List;

public interface PainterDao {

    void insert(Painter painter);

    void delete(int id);

    List<Painter> findAll();

    Painter findById(int id);
}
