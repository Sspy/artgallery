package com.geekhub.repository.dao;

import com.geekhub.repository.entity.User;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
public interface UserDao {

    void save(User user);

    void delete(int id);

    List<User> findAll();

    User findByUsername(String username);
}
