package com.geekhub.repository.dao;

import com.geekhub.repository.entity.News;

import java.util.List;

public interface NewsDao {

    void insert(News news);

    void delete(int id);

    List<News> findAll();

    News findById(int id);

    List<News> getHotNews();
}
