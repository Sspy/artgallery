package com.geekhub.repository.service;


import com.geekhub.repository.entity.User;
import com.geekhub.repository.entity.UserForm;

import java.util.List;

public interface UserService {

    int save(UserForm user);

    void delete(int id);

    List<User> findAll();

    User findUserByUsername(String username);
}
