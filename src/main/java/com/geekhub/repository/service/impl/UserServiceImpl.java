package com.geekhub.repository.service.impl;

import com.geekhub.repository.dao.UserDao;
import com.geekhub.repository.dao.impl.UserJdbcDao;
import com.geekhub.repository.entity.Role;
import com.geekhub.repository.entity.User;
import com.geekhub.repository.entity.UserForm;
import com.geekhub.repository.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserServiceImpl(UserJdbcDao userDao, BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    public int save(UserForm userForm) {
        User user = new User();
        String password = userForm.getPassword();
        String confirmPassword = userForm.getConfirmPassword();

        if (password.equals(confirmPassword)) {
            user.setId(userForm.getId());
            user.setPassword(bCryptPasswordEncoder.encode(userForm.getPassword()));
            user.setUsername(userForm.getUsername());
            user.setEmail(userForm.getEmail());
            user.setRole(Role.USER.toString());
            userDao.save(user);
            return 1;
        }
        return 0;
    }

    @Override
    public void delete(int id) {
        userDao.delete(id);
    }

    @Override
    public List<User> findAll() {
        return userDao.findAll();
    }

    @Override
    public User findUserByUsername(String username) {
        return userDao.findByUsername(username);
    }
}
