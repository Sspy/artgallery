package com.geekhub.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class Picture {

    private int id;
    private String name;
    private String image;
    private String fullImage;
    private String painter;
    private String style;
    private double price;
}
