package com.geekhub.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class UserForm {

    private int id;
    private String username;
    private String password;
    private String confirmPassword;
    private String email;
}
