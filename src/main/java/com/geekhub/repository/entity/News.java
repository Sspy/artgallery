package com.geekhub.repository.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDate;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class News {

    private int id;
    private String name;
    private String review;
    private String text;
    private String image;
    private String fullImage;
    private LocalDate date;
}
