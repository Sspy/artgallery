package com.geekhub.repository.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class SearchForm {

    private String name;
    private String style;
    private Double price;
    private String priority;
}
