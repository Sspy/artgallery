package com.geekhub.repository.entity;

public enum Role {
    USER,
    ADMIN;

    @Override
    public String toString() {
        return super.toString().toUpperCase();
    }
}
